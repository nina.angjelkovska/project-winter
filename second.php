<!DOCTYPE html>

<html>

<head>

    <title>NewWebSite</title>

    <meta charset="utf-8">

    <style type="text/css">

        body
        {
            background-color: #676bef;
        }
        div
        {
            margin:15px;
        }

        input[type=text] 
        {
            width: 40%;
            height: 10%;
            margin: 8px 0;
        }
        .margin
        {
            margin-top: 20px;
        }
        .margin1
        {
            margin-top: 60px;
        }
        .margin2
        {
            margin:10px;
            padding: 40px;
        }
        input[type="submit"]
        {
            color: white;
            background-color: #14489b;
            border-radius: 20px;
            border:1px solid #14489b;
            padding:10px 20px;  
        }
        textarea
        {
            border-radius: 10px;
        }
        .left
        {
            text-align: left;
            margin-left: 29.5%;
        }

    </style>

</head>

<body>
    <center>    

        <h2>Еден чекор ве дели од вашата веб страна</h2>

        <form action="/project/database.php" method="POST" accept-charset="utf-8">
            <div class="margin">
                <div class="left">
                    <b><label>Напишете го линкот до cover сликата:</label></b><br>
                </div>
                <input type="text" name="cover_image" size="60"><br>
            </div>
            <div class="margin"> 
                <div class="left">
                    <b><label>Внесете го насловот:</label></b><br>
                </div>
                <input type="text" name="title" size="60"><br>
            </div>
            <div class="margin">
                <div class="left">
                    <b><label>Внесете го поднасловот:</label></b><br>
                </div>
                <input type="text" name="subheading" size="60"><br>
            </div>
            <div class="margin">
                <div class="left">
                    <b><label>Напишете нешто за вас:</label></b><br>
                </div>
                <textarea name="forus" rows="5" cols="103"></textarea> 
            </div>
            <div class="margin">
                <div class="left">
                    <b><label>Внесете го вашиот телефон:</label></b><br>
                </div>
                <input type="text" name="phone" size="60"><br>  
            </div>
            <div class="margin">
                <div class="left">
                    <b><label>Внесете ја вашата локација:</label></b><br>
                </div>
                <input type="text" name="location" size="60"><br>
            </div>
            <hr/>
            <div class="margin">
                <div class="left">
                    <b><label>Одберете дали нудите сервиси или продукти:</label></b><br>
                </div>
                <select name="service">
                    <option value="servisi">Сервиси</option>
                    <option value="produkti">Продукти</option>
                </select>
            </div>

            <b><p class="margin1">Внесете URL од слика и опис на вашите продукти или сервиси</p></b><br>

            <div class="margin1">
                <div class="left">
                    <b><label>URL од слика</label></b><br> 
                </div> 
                <input type="text" name="img1" size="60"><br>
                <div class="left">
                    <b><label>Опис за сликата</label></b><br>
                </div>
                <textarea name="opis1" rows="5" cols="103"></textarea>
            </div>
            <div class="margin1">
                <div class="left">
                    <b><label>URL од слика</label></b><br> 
                </div> 
                <input type="text" name="img2" size="60"><br>
                <div class="left">
                    <b><label>Опис за сликата</label></b><br>
                </div>
                <textarea name="opis2" rows="5" cols="103"></textarea>
            </div>
            <div class="margin1">
                <div class="left">
                    <b><label>URL од слика</label></b><br> 
                </div> 
                <input type="text" name="img3" size="60"><br>
                <div class="left">
                    <b><label>Опис за сликата</label></b><br>
                </div>
                <textarea name="opis3" rows="5" cols="103"></textarea>
            </div>
            <hr/>
            <div class="pad">
                <b><label>Напишете нешто за вашата фирма што луѓето треба да го знаат пред да ве контактираат</label></b><br>
                <textarea name="foryou" rows="5" cols="103"></textarea>   
            </div>
            <hr/>
            <div class="margin2"><b><label>Внесете ги линковите од вашите социјални мрежи</label></b><br></div>
                <div>
                    <div class="left">
                        <b><label>LinkedIn</label></b><br>
                    </div>
                    <input type="text" name="linkedin" size="60"><br>
                    <div class="left">
                        <b><label>Facebook</label></b><br>
                    </div>
                    <input type="text" name="facebook" size="60"><br>
                    <div class="left">
                        <b><label>Twitter</label></b><br>
                    </div>
                    <input type="text" name="twitter" size="60"><br>
                    <div class="left">
                        <b><label>Google+</label></b><br>
                    </div>
                    <input type="text" name="google" size="60"><br>
                    <br>
                    <input type="submit" value="Потврди">
                </div>
        </form>

    </center>

</body>
</html>