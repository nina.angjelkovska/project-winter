<?php

$username = 'root';
$password = 'root';
$database_host = 'localhost';
$database_name = 'project';
$database_type = 'mysql';

try
{

	$connection = new PDO("$database_type:host=$database_host;dbname=$database_name", $username, $password);

	$statement = $connection->prepare('INSERT INTO page (cover_image, title, subheading, forus, phone, location, 
				service, img1, opis1, img2, opis2, img3, opis3, foryou, linkedin, facebook, twitter, google) 
				VALUES (:cover_image, :title, :subheading, :forus, :phone, :location, :service, :img1, :opis1, 
				:img2, :opis2, :img3, :opis3, :forus, :linkedin, :facebook, :twitter, :google)');

	$statement->bindParam(':cover_image', $_POST['cover_image']);
	$statement->bindParam(':title', $_POST['title']);
	$statement->bindParam(':subheading', $_POST['subheading']);
	$statement->bindParam(':forus', $_POST['forus']);
	$statement->bindParam(':phone', $_POST['phone']);
	$statement->bindParam(':location', $_POST['location']);
	$statement->bindParam(':service', $_POST['service']);
	$statement->bindParam(':img1', $_POST['img1']);
	$statement->bindParam(':opis1', $_POST['opis1']);
	$statement->bindParam(':img2', $_POST['img2']);
	$statement->bindParam(':opis2', $_POST['opis2']);
	$statement->bindParam(':img3', $_POST['img3']);
	$statement->bindParam(':opis3', $_POST['opis3']);
	$statement->bindParam(':foryou', $_POST['foryou']);
	$statement->bindParam(':linkedin', $_POST['linkedin']);
	$statement->bindParam(':facebook', $_POST['facebook']);
	$statement->bindParam(':twitter', $_POST['twitter']);
	$statement->bindParam(':google', $_POST['google']);


$result = $statement->execute();

$id = $connection->lastInsertId();

header('Location: third.php?id='.$id);


}
// catch(PDOException $e){
//     var_dump($e);
}

?>